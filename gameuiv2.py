import pygame
import random

pygame.init()
dimension = 500
divide = 10
units = int(dimension / divide)

window = pygame.display.set_mode((dimension, dimension))

pygame.display.set_caption("Strategy Based Ai Warfare")
wall = pygame.image.load('mountain.jpg')
clock = pygame.time.Clock()
font = pygame.font.SysFont('comicsans', 30)

grid = [['grass' for i in range(units)] for j in range(units)]
wallcount = 0
watercount = 0
resourcecount = 0
respoint = 0
resautotime = 0
resmove = 0
onbase = False
pointdelay = 0


def redrawGameWindow():
    window.blit(wall, (0, 0))  # background wall image
    drawgrass()
    drawwater()
    drawresource()
    drawplayer()
    drawbase()
    text = font.render('Resource: ' + str(resmove), 1, (255,200,255))
    window.blit(text, (340, 10))
    pygame.display.update()


def generatewall():
    global grid, wallcount

    while wallcount < 200:
        i = random.randint(0, units - 1)
        j = random.randint(0, units - 1)
        hv = random.randint(0, 1)
        if hv == 0:
            for x in range(4):
                for y in range(1):
                    if i + x < units and j + y < units:
                        grid[i + x][j + y] = 'wall'
                        wallcount += 1
        else:
            for x in range(1):
                for y in range(4):
                    if i + x < units and j + y < units:
                        grid[i + x][j + y] = 'wall'
                        wallcount += 1


def generatewater():
    global grid, watercount

    while watercount < 800:
        i = random.randint(0, units - 1)
        j = random.randint(0, units - 1)
        hv = random.randint(0, 1)
        if hv == 0:
            for x in range(4):
                for y in range(6):
                    if i + x < units and j + y < units:
                        if grid[i + x][j + y] != 'wall' and grid[i][j] != 'wall':
                            grid[i + x][j + y] = 'water'
                            watercount += 1
        else:
            for x in range(6):
                for y in range(4):
                    if i + x < units and j + y < units:
                        if grid[i + x][j + y] != 'wall' and grid[i][j] != 'wall':
                            grid[i + x][j + y] = 'water'
                            watercount += 1


def generateresource():
    global grid, resourcecount

    while resourcecount < 10:
        i = random.randint(0, units - 1)
        j = random.randint(0, units - 1)
        if grid[i][j] != 'wall':
            grid[i][j] = 'resource'
            resourcecount += 1


def playerstart():
    global grid
    i = random.randint(0, units - 1)
    j = random.randint(0, units - 1)
    while True:
        if i > units - 2:
            i = random.randint(0, units - 1)
            j = random.randint(0, units - 1)
        if grid[i][j] == 'grass':
            grid[i][j] = 'player'
            break
        else:
            i += 2


def drawgrass():
    global grid
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'grass':
                pygame.draw.rect(window, (0, 50, 0), (i * divide, j * divide, divide, divide))


def drawwater():
    global grid
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'water':
                pygame.draw.rect(window, (0, 0, 255), (i * divide, j * divide, divide, divide))


def drawplayer():
    global grid
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'player' or grid[i][j] == 'player+base':
                pygame.draw.rect(window, (255, 0, 0), (i * divide, j * divide, divide, divide))

def drawresource():
    global grid
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'resource':
               pygame.draw.circle(window, (255, 255, 255), (int(i*divide + divide/2), int(j*divide + divide/2)), (int(divide/2)))
#              pygame.draw.rect(window, (255, 255, 255), (i * divide, j * divide, divide, divide))

def drawbase():
    global grid
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'base':
               pygame.draw.circle(window, (255, 0, 0), (int(i*divide + divide/2), int(j*divide + divide/2)), (int(divide/2)))

def moveup():
    global grid, respoint, onbase, resmove
    resmove += 50 * respoint
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'player':
                x, y = i, j
            if grid[i][j] == 'player+base':
                x, y = i, j
                onbase = True
    if onbase:
        if grid[x][y-1] == 'grass':
            grid[x][y] = 'base'
            grid[x][y-1] = 'player'
            onbase = False
    else:
        if y-1 < 0:
            return
        if grid[x][y-1] == 'grass':
            grid[x][y], grid[x][y-1] = grid[x][y-1], grid[x][y]
        elif grid[x][y-1] == 'resource':
            grid[x][y] = 'grass'
            grid[x][y-1] = 'player+base'
            respoint += 1

def movedown():
    global grid, respoint, onbase, resmove
    resmove += 50 * respoint
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'player':
                x, y = i, j
            if grid[i][j] == 'player+base':
                x, y = i, j
                onbase = True
    if onbase:
        if grid[x][y+1] == 'grass':
            grid[x][y] = 'base'
            grid[x][y+1] = 'player'
            onbase = False
    else:
        if y+1 > 49:
            return
        if grid[x][y+1] == 'grass':
            grid[x][y], grid[x][y+1] = grid[x][y+1], grid[x][y]
        elif grid[x][y+1] == 'resource':
            grid[x][y] = 'grass'
            grid[x][y+1] = 'player+base'
            respoint += 1

def moveleft():
    global grid, respoint, onbase, resmove
    resmove += 50 * respoint
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'player':
                x, y = i, j
            if grid[i][j] == 'player+base':
                x, y = i, j
                onbase = True
    if onbase:
        if grid[x-1][y] == 'grass':
            grid[x][y] = 'base'
            grid[x-1][y] = 'player'
            onbase = False
    else:
        if x-1 < 0:
            return
        if grid[x-1][y] == 'grass':
            grid[x][y], grid[x-1][y] = grid[x-1][y], grid[x][y]
        elif grid[x-1][y] == 'resource':
            grid[x][y] = 'grass'
            grid[x-1][y] = 'player+base'
            respoint += 1

def moveright():
    global grid, respoint, onbase, resmove
    resmove += 50 * respoint
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'player':
                x, y = i, j
            if grid[i][j] == 'player+base':
                x, y = i, j
                onbase = True
    if onbase:
        if grid[x+1][y] == 'grass':
            grid[x][y] = 'base'
            grid[x+1][y] = 'player'
            onbase = False
    else:
        if x+1 > 49:
            return
        if grid[x+1][y] == 'grass':
            grid[x][y], grid[x+1][y] = grid[x+1][y], grid[x][y]
        elif grid[x+1][y] == 'resource':
            grid[x][y] = 'grass'
            grid[x+1][y] = 'player+base'
            respoint += 1


def generatemap():
    global grid
    grid = [['grass' for i in range(units)] for j in range(units)]
    generatewall()
    generatewater()
    generateresource()
    playerstart()

"""for i in range(units):
    for j in range(units):
        print(grid[i][j])"""

generatemap()
run = True
while run:
    clock.tick(10)
    redrawGameWindow()

    '''if pointdelay < 10:
        pointdelay += 1
    else:
        resautotime += respoint * 50
        pointdelay = 0'''
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False



    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        moveleft()
    elif keys[pygame.K_RIGHT]:
        moveright()
    elif keys[pygame.K_DOWN]:
        movedown()
    elif keys[pygame.K_UP]:
        moveup()
    elif keys[pygame.K_SPACE]:
        generatemap()

pygame.quit()
