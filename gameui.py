import pygame
import random

pygame.init()
dimension = 500
divide = 10
units = int(dimension / divide)

window = pygame.display.set_mode((dimension, dimension))

pygame.display.set_caption("Strategy Based Ai Warfare")
wall = pygame.image.load('mountain.jpg')
clock = pygame.time.Clock()

grid = [['grass' for i in range(units)] for j in range(units)]
wallcount = 0
watercount = 0
resourcecount = 0


def redrawGameWindow():
    window.blit(wall, (0, 0))  # background wall image
    drawgrass()
    drawwater()
    drawresource()
    drawplayer()
    pygame.display.update()


def generatewall():
    global grid, wallcount

    while wallcount < 200:
        i = random.randint(0, units - 1)
        j = random.randint(0, units - 1)
        hv = random.randint(0, 1)
        if hv == 0:
            for x in range(4):
                for y in range(1):
                    if i + x < units and j + y < units:
                        grid[i + x][j + y] = 'wall'
                        wallcount += 1
        else:
            for x in range(1):
                for y in range(4):
                    if i + x < units and j + y < units:
                        grid[i + x][j + y] = 'wall'
                        wallcount += 1


def generatewater():
    global grid, watercount

    while watercount < 800:
        i = random.randint(0, units - 1)
        j = random.randint(0, units - 1)
        hv = random.randint(0, 1)
        if hv == 0:
            for x in range(4):
                for y in range(6):
                    if i + x < units and j + y < units:
                        if grid[i + x][j + y] != 'wall' and grid[i][j] != 'wall':
                            grid[i + x][j + y] = 'water'
                            watercount += 1
        else:
            for x in range(6):
                for y in range(4):
                    if i + x < units and j + y < units:
                        if grid[i + x][j + y] != 'wall' and grid[i][j] != 'wall':
                            grid[i + x][j + y] = 'water'
                            watercount += 1


def generateresource():
    global grid, resourcecount

    while resourcecount < 10:
        i = random.randint(0, units - 1)
        j = random.randint(0, units - 1)
        if grid[i][j] != 'wall':
            grid[i][j] = 'resource'
            resourcecount += 1


def playerstart():
    global grid
    i = random.randint(0, units - 1)
    j = random.randint(0, units - 1)
    while True:
        if i > units:
            i = random.randint(0, units - 1)
            j = random.randint(0, units - 1)
        if grid[i][j] == 'grass':
            grid[i][j] = 'player'
            break
        else:
            i += 2


def drawgrass():
    global grid
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'grass':
                pygame.draw.rect(window, (0, 50, 0), (i * divide, j * divide, divide, divide))


def drawwater():
    global grid
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'water':
                pygame.draw.rect(window, (0, 0, 255), (i * divide, j * divide, divide, divide))


def drawplayer():
    global grid
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'player':
                pygame.draw.rect(window, (255, 0, 0), (i * divide, j * divide, divide, divide))

def drawresource():
    global grid
    for i in range(units):
        for j in range(units):
            if grid[i][j] == 'resource':
               pygame.draw.circle(window, (255, 255, 255), (int(i*divide + divide/2), int(j*divide + divide/2)), (int(divide/2)))
               #  pygame.draw.rect(window, (255, 255, 255), (i * divide, j * divide, divide, divide))


generatewall()
generatewater()
generateresource()
playerstart()
run=True
while run:
    clock.tick(27)
    redrawGameWindow()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

pygame.quit()
