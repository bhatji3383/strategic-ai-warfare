import random
import numpy as np
import sys
import pygame

def cellogic(arr,size):
    change = [[0 for i in range(size)] for j in range(size)]
    for i in range(size):
        for j in range(size):
            count = 0
            lf = (i-1)%size
            rt = (i+1)%size
            up = (j-1)%size
            dn = (j+1)%size
            if arr[lf][up] == '#':
                count += 1
            if arr[lf][j] == '#':
                count += 1
            if arr[lf][dn] == '#':
                count += 1
            if arr[rt][up] == '#':
                count += 1
            if arr[rt][j] == '#':
                count += 1
            if arr[rt][dn] == '#':
                count += 1
            if arr[i][up] == '#':
                count += 1
            if arr[i][dn] == '#':
                count += 1
            #if arr[i][j] == '#' and count < 2:
                #change[i][j] = 1
            if arr[i][j] == '#' and count < 5:
                change[i][j] = 1
            if arr[i][j] == ' ' and count > 6:
                change[i][j] = 1
    for i in range(size):
        for j in range(size):
            if change[i][j] == 1:
                if arr[i][j] == ' ':
                    arr[i][j] = '#'
                elif arr[i][j] == '#':
                    arr[i][j] = ' '
    
    return arr


class Player():
    def __init__(self,pid):
        self.pid = pid
        self.resource = 0
        self.stat = 1

    def incRes(self, claim):
        self.resource += claim*200
    
    def updateStat(self,gmap):
        if 'wl:' + str(self.pid) not in  gmap:
            self.stat = 0
        return self.stat

    
class Settlement:
    def __init__(self,pos):
        self.pos = pos
        self.uid = 's'
        self.pid = 0
    
    def claimSetl(self,pid):
        self.uid = 's:' + str(pid)
        self.pid = pid
        return self

    def createSld(self,pid,gmap):
        for i in range(8):
            x = random.randint(-1,1)
            y = random.randint(-1,1)
            if x==0 and y==0:
                y = 1
            pos = ((self.pos[0]+x) % 64,(self.pos[1]+y) % 64)
            if(gmap[pos] == ' '):
                return Soldier('sld:'+str(pid),pos)
        return None

    




class Warlord():
    def __init__(self, uid, pos):
        self.uid = uid
        self.pos = pos

    def makeMove(self,dirn,gmap):
        mv = (1,0) if dirn == 0 else (0,1) if dirn == 1 else (-1,0) if dirn == 2 else (0,-1) if dirn == 3 else (0,0)
        if(gmap[(self.pos[0]+mv[0])%64,(self.pos[1]+mv[1])%64] == '#'):
            mv = (0,0)
        self.pos = ((self.pos[0]+mv[0])%64,(self.pos[1]+mv[1])%64)

    def getSurroundings(self,gmap):
        x = np.arange(0,64)
        y = np.arange(0,64)
        cx,cy = self.pos
        r=8
        mask = (x[np.newaxis,:] - cx) ** 2 + (y[:,np.newaxis] - cy) ** 2 <= r**2
        return gmap[mask]


class Soldier():
    def __init__(self,uid,pos):
        self.uid = uid
        self.pos = pos
    
    def makeMove(self,dirn,gmap):
        mv = (1,0) if dirn == 0 else (0,1) if dirn == 1 else (-1,0) if dirn == 2 else (0,-1) if dirn == 3 else (0,0)
        if(gmap[(self.pos[0]+mv[0])%64,(self.pos[1]+mv[1])%64] == '#' or gmap[(self.pos[0]+mv[0])%64,(self.pos[1]+mv[1])%64].startswith('sld') or gmap[(self.pos[0]+mv[0])%64,(self.pos[1]+mv[1])%64].startswith('s:')):
            mv = (0,0)
        self.pos = ((self.pos[0]+mv[0])%64,(self.pos[1]+mv[1])%64)

    def getSurroundings(self,gmap):
        x = np.arange(0,64)
        y = np.arange(0,64)
        cx,cy = self.pos
        r=8
        mask = (x[np.newaxis,:] - cx) ** 2 + (y[:,np.newaxis] - cy) ** 2 <= r**2
        return np.array(gmap[mask],dtype = object)


def getWarlord(gmap,pid):
    i,j = np.where(gmap == 'wl:'+str(pid)) if 'wl:'+str(pid) in gmap else np.where(gmap == 's:wl:'+str(pid))
    pos = (i[0],j[0])
    return Warlord('wl:'+str(pid),pos)

def getSoldiers(gmap,pid):
    i,j = np.where(gmap == 'sld:'+str(pid)) 
    sld = []
    for k in range(i.size):
        pos = (i[k],j[k])
        sld.append(Soldier('sld:'+str(pid),pos))
    return sld

def getSettlements(gmap,pid):
    i,j = np.where(gmap == 's:'+str(pid))
    k,l = np.where(gmap=='s:sld:'+str(pid)) #sldpartsssss
    n,o = np.where(gmap=='s:wl:'+str(pid))
    setl = []
    
    for m in range(i.size):
        pos = (i[m],j[m])
        setl.append(Settlement(pos).claimSetl(pid))
    
    for m in range(k.size):
        pos = (k[m],l[m])
        setl.append(Settlement(pos).claimSetl(pid))
    
    for m in range(n.size):
        pos = (n[m],o[m])
        setl.append(Settlement(pos).claimSetl(pid))

    return setl


def gBot(gmap,pid,pstatus):
    wl = getWarlord(gmap,pid)
    soldiers = getSoldiers(gmap,pid)
    csettlements = getSettlements(gmap,pid)
    cque = []
   
    
    if(gmap[wl.pos] != 's:wl:'+str(pid)):
        wl.makeMove(random.randint(0,3),gmap)
        
    cque.append(wl)

    for sld in soldiers:
        g = 0
        for i in csettlements:
            if(i.pos == sld.pos):
                g=1
                break
                    
        if g == 0:    
            sld.makeMove(random.randint(0,3),gmap)
        cque.append(sld)

    if pstatus.resource >= 500 and len(csettlements)>0:
        cque.append(csettlements[0].createSld(pid,gmap))

    return cque

def redrawGameWindow(grid):
    for i in range(64):
        for j in range(64):
            if grid[i][j] == '#':
                pygame.draw.rect(window, (150, 75, 0), (i * divide, j * divide, divide, divide))
            elif grid[i][j] == ' ':
                pygame.draw.rect(window, (155, 135, 12), (i * divide, j * divide, divide, divide))
            elif grid[i][j] == 's':
                pygame.draw.rect(window, (192, 192, 192), (i * divide, j * divide, divide, divide))
            elif grid[i][j] == 'wl:1000':
                pygame.draw.rect(window, (255, 0, 0), (i * divide, j * divide, divide, divide))
            elif grid[i][j] == 'wl:2000':
                pygame.draw.rect(window, (0, 0, 255), (i * divide, j * divide, divide, divide))
            elif grid[i][j] == 'sld:1000':
                pygame.draw.rect(window, (100, 0, 0), (i * divide, j * divide, divide, divide))
            elif grid[i][j] == 'sld:2000':
                pygame.draw.rect(window, (0, 0, 100), (i * divide, j * divide, divide, divide))
            elif grid[i][j] == 's:1000':
                pygame.draw.rect(window, (255, 0, 0), (i * divide, j * divide, divide, divide))
            elif grid[i][j] == 's:2000':
                pygame.draw.rect(window, (0, 0, 255), (i * divide, j * divide, divide, divide))
            elif grid[i][j] == 's:wl:1000':
                pygame.draw.rect(window, (255, 0, 0), (i * divide, j * divide, divide, divide))
            elif grid[i][j] == 's:wl:2000':
                pygame.draw.rect(window, (0, 0, 255), (i * divide, j * divide, divide, divide))
            elif grid[i][j] == 's:sld:1000':
                pygame.draw.rect(window, (255, 0, 0), (i * divide, j * divide, divide, divide))
            elif grid[i][j] == 's:sld:2000':
                pygame.draw.rect(window, (0, 0, 255), (i * divide, j * divide, divide, divide))
    pygame.display.update()


if __name__ == '__main__':
    np.set_printoptions(threshold = sys.maxsize)

    pygame.init()

    dimension = 640
    size = 64
    divide = int(dimension / size)
    gmap = [[' ' for i in range(size)] for j in range(size)]

    window = pygame.display.set_mode((dimension, dimension))
    clock = pygame.time.Clock()

    pygame.display.set_caption("Strategy Based Ai Warfare")

    for i in range(size):
        for j in range(size):
            prob = random.randint(1,100)
            if prob > 45:
                gmap[i][j] = '#'

    gmap = cellogic(gmap,size)  #map generation

    gmap = np.array(gmap,dtype = object)
    
    #generating player ids
    player_1 = 1000
    player_2 = 2000

    #generating player statuses
    pl1 = Player(player_1)
    pl2 = Player(player_2)

    #generating settlements
    for i in range(64):
        while(1):
            i = random.randint(0,int(size)-1)
            j = random.randint(0, int(size)-1)
            if gmap[i][j] != ' ':
                continue
            gmap[i][j] = 's'
            break
    gmapb = np.array(gmap,dtype=object)
    
    #creating warlords for each player
    while(1):
        i = random.randint(0,int(size)-1)
        j = random.randint(0, int(size/2)-1)
        if gmap[i][j] != ' ':
            continue
        gmap[i][j] = 'wl:' + str(player_1)
        break

    while(1):
        i = random.randint(0,int(size)-1)
        j = random.randint(int(size/2),size-1)
        if (gmap[i][j] != ' '):
            continue
        gmap[i][j] = 'wl:' + str(player_2)
        break

    gmapc = np.array(gmapb,dtype=object)

    close = False

    for i in range(500):

        clock.tick(20)
        redrawGameWindow(gmap)

        p1c = gBot(gmap,player_1,pl1)
        p2c = gBot(gmap,player_2,pl2)
        
        for i in p1c:
            if(i==None):
                continue
            if gmapb[i.pos] == 's':
                gmapb[i.pos] = 's:' + i.uid
                continue
            gmapb[i.pos] = i.uid
        
        for i in p2c:
            if(i==None):
                continue
            if gmapb[i.pos] == 's':
                gmapb[i.pos] = 's:' + i.uid
                continue
            gmapb[i.pos] = i.uid

        for i in p1c:
            if(i==None):
                continue
            if(i.uid == 'sld'+str(player_1) or i.uid == 'wl:'+str(player_1)):
                l = i.getSurroundings(gmapb)
                enemyc = 0
                alliedc = 0
                
                for j in l:
                    if j == 'sld:'+str(player_2) or j=='s:sld'+str(player_2) or j=='s:wl:'+str(player_2):
                        enemyc+=1
                    elif j== 'sld:'+str(player_1) or j=='s:sld:'+str(player_1) or j=='s:wl:'+str(player_1):
                        alliedc+=1
                
                if(enemyc>alliedc):
                        gmapb[i.pos] = ' '
        
        for i in p2c:
            if(i==None):
                continue
            if(i.uid == 'sld'+str(player_2) or i.uid == 'wl:'+str(player_2)):
                l = i.getSurroundings(gmapb)
                enemyc = 0
                alliedc = 0
                
                for j in l:
                    if j == 'sld:'+str(player_1) or j=='s:sld'+str(player_1) or j=='s:wl:'+str(player_1):
                        enemyc+=1
                    elif j== 'sld:'+str(player_2) or j=='s:sld:'+str(player_2) or j=='s:wl:'+str(player_2):
                        alliedc+=1
                
                if(enemyc>alliedc):
                        gmapb[i.pos] = ' '

        pl1.incRes(len(getSettlements(gmapb,player_1)))
        pl2.incRes(len(getSettlements(gmapb,player_2)))

        gmap = np.array(gmapb,dtype=object)
        gmapb = np.array(gmapc,dtype = object)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                close = True

        if close:
            break

    print("player1: ", pl1.resource)
    print("player2: ", pl2.resource)

    if(pl1.resource > pl2.resource): 
        print('player1 won')
    elif(pl1.resource < pl2.resource): 
        print('player2 won')
    else: print('its a draw')

    pygame.quit()
        
        #draw gmap : ' ' for floor, '#' for wall, 'wl:<pid>' for warlord, 'sld:<pid>' for soldier, 's' for free settlement, 's:wl:<pid>' for claimed warlord settlement
        # 's:sld:<pid>' for claimed soldier settlement.
        # do it with delay  
        
