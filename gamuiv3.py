import pygame
import random
from assets import cellauto,

if __name__ == "__main__":
    size = 100
    pygame.init()
    dimension = 1400
    divide = 14
    units = int(dimension / divide)

    arr = [[' '  for i in range(size)] for j in range(size)]
    for i in range(size):
        for j in range(size):
            prob = random.randint(1,100)
            if prob > 45:
                arr[i][j] = '#'

    for i in range(5):
        arr = cellauto.cellogic(arr,size)

    window = pygame.display.set_mode((dimension, dimension))

    pygame.display.set_caption("Strategy Based Ai Warfare")

    wallimg = [pygame.image.load('Sprites/wall1.png'), pygame.image.load('Sprites/wall2.png'), pygame.image.load('Sprites/wall3.png'), pygame.image.load('Sprites/wall4.png'), pygame.image.load('Sprites/wall5.png'), pygame.image.load('Sprites/wall6.png')]
    walldimg = [pygame.image.load('Sprites/walld1.png'), pygame.image.load('Sprites/walld2.png'), pygame.image.load('Sprites/walld3.png'), pygame.image.load('Sprites/walld4.png'), pygame.image.load('Sprites/walld5.png'), pygame.image.load('Sprites/walld6.png')]
    grassimg = [pygame.image.load('Sprites/grass1.png'), pygame.image.load('Sprites/grass2.png'), pygame.image.load('Sprites/grass3.png'), pygame.image.load('Sprites/grass4.png'), pygame.image.load('Sprites/grass5.png'), pygame.image.load('Sprites/grass6.png')]
    waterimg = [pygame.image.load('Sprites/water.png'), pygame.image.load('Sprites/waterd.png')]
    playerimg = [pygame.image.load('Sprites/player0.png').convert_alpha(), pygame.image.load('Sprites/player1.png').convert_alpha(), pygame.image.load('Sprites/player2.png').convert_alpha(), pygame.image.load('Sprites/player3.png').convert_alpha()]

    clock = pygame.time.Clock()
    font = pygame.font.SysFont('comicsans', 30)

    grid = arr
    

    class Grass(object):
        def __init__(self, img, x, y):
            self.id = 'grass'
            self.img = img
            self.x = x
            self.y = y

        def draw(self, window):
            window.blit(grassimg[self.img], (self.x * divide, self.y * divide))


    class Water(object):
        def __init__(self, img, x, y):
            self.id = 'water'
            self.img = img
            self.x = x
            self.y = y

        def draw(self, window):
            window.blit(waterimg[self.img], (self.x * divide, self.y * divide))


    class Wall(object):
        def __init__(self, img, x, y):
            self.id = 'wall'
            self.img = img
            self.x = x
            self.y = y

        def draw(self, window):
            window.blit(wallimg[self.img], (self.x * divide, self.y * divide))

        def drawd(self, window):
            window.blit(walldimg[self.img], (self.x * divide, self.y * divide))


    class Player(object):
        def __init__(self, img, x, y):
            self.id = 'player'
            self.img = img
            self.dir = 2
            self.x = x
            self.y = y

        def draw(self, window):
            pygame.draw.circle(window, (210, 210, 210 ,0), (int(self.x* divide + divide / 2), int(self.y*divide + divide / 2)), (42))
            window.blit(playerimg[self.dir], (self.x * divide, self.y * divide))

        def moveup(self):
            global grid
            for i in range(units):
                for j in range(units):
                    if grid[i][j].id == 'player':
                        grid[i][j].dir = 0
                        if j > 0:
                            if grid[i][j-1].id == 'grass':
                                grid[i][j], grid[i][j-1] = grid[i][j-1], grid[i][j]
                                grid[i][j].img, grid[i][j - 1].img = grid[i][j - 1].img, grid[i][j].img
                                grid[i][j].x, grid[i][j - 1].x = grid[i][j - 1].x, grid[i][j].x
                                grid[i][j].y, grid[i][j - 1].y = grid[i][j - 1].y, grid[i][j].y

        def movedown(self):
            global grid
            for x in range(units):
                for y in range(units):
                    if grid[x][y].id == 'player':
                        grid[x][y].dir = 2
                        i, j = x, y
            if j < 49:
                if grid[i][j + 1].id == 'grass':
                    grid[i][j], grid[i][j + 1] = grid[i][j + 1], grid[i][j]
                    grid[i][j].img, grid[i][j + 1].img = grid[i][j + 1].img, grid[i][j].img
                    grid[i][j].x, grid[i][j + 1].x = grid[i][j + 1].x, grid[i][j].x
                    grid[i][j].y, grid[i][j + 1].y = grid[i][j + 1].y, grid[i][j].y

        def moveleft(self):
            global grid
            for i in range(units):
                for j in range(units):
                    if grid[i][j].id == 'player':
                        grid[i][j].dir = 3
                        if i > 0:
                            if grid[i - 1][j].id == 'grass':
                                grid[i][j], grid[i - 1][j] = grid[i - 1][j], grid[i][j]
                                grid[i][j].img, grid[i - 1][j].img = grid[i - 1][j].img, grid[i][j].img
                                grid[i][j].x, grid[i - 1][j].x = grid[i - 1][j].x, grid[i][j].x
                                grid[i][j].y, grid[i - 1][j].y = grid[i - 1][j].y, grid[i][j].y

        def moveright(self):
            global grid
            br = False
            for x in range(units):
                for y in range(units):
                    if grid[x][y].id == 'player':
                        grid[x][y].dir = 1
                        i, j = x, y

            if i < 49:
                if grid[i + 1][j].id == 'grass':
                    grid[i][j], grid[i + 1][j] = grid[i + 1][j], grid[i][j]
                    grid[i][j].img, grid[i + 1][j].img = grid[i + 1][j].img, grid[i][j].img
                    grid[i][j].x, grid[i + 1][j].x = grid[i + 1][j].x, grid[i][j].x
                    grid[i][j].y, grid[i + 1][j].y = grid[i + 1][j].y, grid[i][j].y


    def drawshadow():
        for i in range(units):
            for j in range(units):
                if j < 49:
                    if grid[i][j].id == 'wall' and grid[i][j+1].id != 'wall':
                        grid[i][j].drawd(window)
                else:
                    if grid[i][j].id == 'wall':
                        grid[i][j].drawd(window)
        for i in range(units):
            for j in range(units):
                if j > 0:
                    if grid[i][j].id == 'water' and grid[i][j-1].id != 'water':
                        grid[i][j].img = 1
                        grid[i][j].draw(window)
                else:
                    if grid[i][j].id == 'water':
                        grid[i][j].draw(window)


    def redrawGameWindow():
        #  window.blit(wall, (0, 0))  # background wall image
        for i in range(units):
            for j in range(units):
                grid[i][j].draw(window)
        drawshadow()
        for i in range(units):
            for j in range(units):
                if grid[i][j].id == 'player':
                    grid[i][j].draw(window)

        pygame.display.update()


    def generategrass():
        for i in range(100):
            for j in range(100):
                if(grid[i][j] != '#'):
                    img = random.randint(0, 5)
                    grid[i][j] = Grass(img,i,j)


    def generatewall():
        for i in range(100):
            for j in range(100):
                if(grid[i][j] == '#'):
                    img = random.randint(0, 5)
                    grid[i][j] = Wall(img,i,j)




    def generateplayer():
        global grid
        i = random.randint(0, units - 1)
        j = random.randint(0, units - 1)
        while grid[i][j].id != 'grass':
            i = random.randint(0, units - 1)
            j = random.randint(0, units - 1)
            if grid[i][j].id == 'grass':
                break
        grid[i][j] = Grass(0, i, j)
        grid[i][j].draw(window)
        grid[i][j] = Player(2, i, j)


    generategrass()
    generatewall()
    generateplayer()


    run = True
    while run:
        clock.tick(10)
        redrawGameWindow()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            for i in range(units):
                for j in range(units):
                    if grid[i][j].id == 'player':
                        grid[i][j].moveleft()
        elif keys[pygame.K_RIGHT]:
            for i in range(units):
                for j in range(units):
                    if grid[i][j].id == 'player':
                        x, y = i, j
            grid[x][y].moveright()
        elif keys[pygame.K_DOWN]:
            for i in range(units):
                for j in range(units):
                    if grid[i][j].id == 'player':
                        x, y = i, j
            grid[x][y].movedown()
        elif keys[pygame.K_UP]:
            for i in range(units):
                for j in range(units):
                    if grid[i][j].id == 'player':
                        grid[i][j].moveup()

    pygame.quit()


